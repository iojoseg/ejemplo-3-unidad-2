import javax.swing.JOptionPane;
import java.text.DecimalFormat;

public class principalConversor{
  
  public static void main(String args[]){
  
    String opc="";
    int opc1;  
    DecimalFormat formateador = new DecimalFormat("###,###.##");
    ConversorDeDivisas  converD = new ConversorDeDivisas();
    ConversorDeDivisas  converD2 = new ConversorDeDivisas(100.0);
    double Euro = 24.38, Dls =20.70;
    
    do{
    
    opc = JOptionPane.showInputDialog(null, "Menu de Conversion de Divisas \n" +
                                                    "1.- Convertir de Pesos a Dolares \n" +
                                                    "2.- Convertir de Pesos a Euros  \n" +
                                                    "3.- Convertir de Euros a Pesos \n" +
                                                    "4.- Convetir de Dolares a Pesos \n" +
                                                    "5.- Salir (Terminar el programa)");
    
    /* Evaluar(validar, verificar) 
    con un if el tipo de dato de la variable
     antes de convertirla a entero.*/
     
    opc1= Integer.parseInt(opc);
    
   
                                                    				    
    switch(opc1){
    
    case 1:
           double var = entradaDatos("Ingrese la cantidad de pesos a convertir Dolares");
           converD.setTipoCambio(Dls);
           JOptionPane.showMessageDialog(null, "Su conversion de Pesos a Dls "+ formateador.format(converD.pesosToDolar(var))+" dls ");
           break;
           
    case 2:
           double var2 = entradaDatos("Ingrese la cantidad de pesos a convertir Euros");
           converD.setTipoCambio(Euro);
           JOptionPane.showMessageDialog(null, "Su conversion de Pesos a Euros "+ formateador.format(converD.pesosToEuros(var2))+" Euros ");
           break;
    
    case 3:
           String valor3 = JOptionPane.showInputDialog(null,"Ingrese la cantidad de Euros a convertir Pesos(MX)"); 
           double var3 = Double.parseDouble(valor3);
           converD.setTipoCambio(Euro);
           JOptionPane.showMessageDialog(null, "Su conversion de Euros a Pesos "+ formateador.format(converD.eurosToPesos(var3))+" Pesos(MX)");
           break;
    
    case 4:
           String valor4 = JOptionPane.showInputDialog(null,"Ingrese la cantidad de Dolares a convertir Pesos(MX)"); 
           double var4 = Double.parseDouble(valor4);
           converD.setTipoCambio(Dls);
           JOptionPane.showMessageDialog(null, "Su conversion de Dolares a Pesos "+ formateador.format(converD.dolarToPesos(var4))+" Pesos(MX)");
           break;

    case 5: 
           JOptionPane.showMessageDialog(null, "Gracias por utilizar nuestra aplicacion ...  ");
           System.out.println("Gracias por utilizar nuestra aplicacion");
           break;
    
    default:  
             JOptionPane.showMessageDialog(null, "Opcion No Valida ...  ");
             System.out.println("Opcion no validad...");
             break;


    }
    
    
    
    }while(opc1 != 5);
    
   
  }
  
  /* Crear algunos metodos para JOptionPane.showMessageDialog,
     JOptionPane.showInputDialog
  */
 public static double entradaDatos(String title){
   //"Ingrese la cantidad de pesos a convertir Dolares"
    String valor1 = JOptionPane.showInputDialog(null,title); 
    double var1 = Double.parseDouble(valor1);
    return var1;
           
 }

}